// Package popua provides methods to get random popular user agent strings.
package popua

import (
	"hash/fnv"

	"github.com/1set/gut/yrand"
)

//go:generate go run regen/gendata.go -input browsers.json

// ListAll returns a list of popular user agent strings.
func ListAll() []string {
	return userAgents
}

// GetRandom returns a random popular user agent string.
func GetRandom() string {
	s, _ := yrand.ChoiceString(userAgents)
	return s
}

// GetBySeed returns a random popular user agent string, the same seed gets the same result.
func GetBySeed(seed string) string {
	return getElementBySeed(userAgents, seed)
}

// GetWeightedRandom returns a weighted random popular user agent string.
func GetWeightedRandom() string {
	s, _ := yrand.ChoiceString(weightedUserAgents)
	return s
}

// GetWeightedBySeed returns a weighted random popular user agent string, the same seed gets the same result.
func GetWeightedBySeed(seed string) string {
	return getElementBySeed(weightedUserAgents, seed)
}

func getElementBySeed(list []string, seed string) string {
	idx := hashStr(seed) % uint32(len(list))
	return list[idx]
}

func hashStr(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}
