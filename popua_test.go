package popua

import (
	"fmt"
	"testing"

	"github.com/1set/gut/ystring"
)

func TestBySeed(t *testing.T) {
	tests := []struct {
		name   string
		method func(string) string
	}{
		{"GetBySeed", GetBySeed},
		{"GetWeightedBySeed", GetWeightedBySeed},
	}
	seeds := []string{
		"",
		"1",
		"2",
		"abc",
	}
	for _, tt := range tests {
		for _, s := range seeds {
			t.Run(fmt.Sprintf("%s with seed=%q", tt.name, s), func(t *testing.T) {
				var result string
				for i := 1; i <= 5; i++ {
					got := tt.method(s)
					if ystring.IsBlank(got) {
						t.Errorf("GetBySeed() = %v, want non-blank string", got)
					}
					if ystring.IsNotBlank(result) {
						if result != got {
							t.Errorf("GetBySeed() = %v, want %v", got, result)
						}
					} else {
						result = got
					}
				}
			})
		}
	}
}

func TestRandom(t *testing.T) {
	tests := []struct {
		name   string
		method func() string
	}{
		{"GetRandom", GetRandom},
		{"GetWeightedRandom", GetWeightedRandom},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%s", tt.name), func(t *testing.T) {
			for i := 1; i <= 5; i++ {
				got := tt.method()
				if ystring.IsBlank(got) {
					t.Errorf("GetBySeed() = %v, want non-blank string", got)
				}
			}
		})
	}
}

func TestListAll(t *testing.T) {
	got := ListAll()
	if len(got) <= 0 {
		t.Errorf("ListAll() = %v, want non-empty list", got)
	}

	for idx, ua := range got {
		if ystring.IsBlank(ua) {
			t.Errorf("ListAll()[%d] = %q, want non-empty string", idx, ua)
		}
	}
}

func BenchmarkGetBySeed(b *testing.B) {
	var seed = "aacdfa688fef76e178138f7985738e236369445a"
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = GetBySeed(seed)
	}
}

func BenchmarkGetWeightedBySeed(b *testing.B) {
	var seed = "aacdfa688fef76e178138f7985738e236369445a"
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = GetWeightedBySeed(seed)
	}
}

func BenchmarkGetRandom(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = GetRandom()
	}
}

func BenchmarkGetWeightedRandom(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = GetWeightedRandom()
	}
}

func BenchmarkListAll(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = ListAll()
	}
}
