# popua [![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/popua)

popua, stands for **Pop**ular **U**ser **A**gent, is a generated go package provides popular user agent strings.

```
    ____              __  _____
   / __ \____  ____  / / / /   |
  / /_/ / __ \/ __ \/ / / / /| |
 / ____/ /_/ / /_/ / /_/ / ___ |
/_/    \____/ .___/\____/_/  |_|
           /_/
```

Data was retrieved from [this page](https://techblog.willshouse.com/2012/01/03/most-common-user-agents/) in JSON format and saved as **user-agent.json**.

Mirror: https://github.com/fake-useragent/fake-useragent/blob/master/src/fake_useragent/data/browsers.json

## install

```bash
go get -d bitbucket.org/ai69/popua
```
